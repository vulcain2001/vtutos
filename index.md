# **vtutos**

This repos contains various tutorials that I found were missing from human kind's common knowledge (_i.e. Internet_).

![WTFPL Badge](./.ressources/wtfpl-badge.png)

## Categories

- _**[qubes](./qubes/) :** tutorials concerning **[Qubes OS](https://www.qubes-os.org/)**._
- _**[shell](./shell/) :** boost your productivity on **Linux** using [**Shell** scripts](https://en.wikipedia.org/wiki/Shell_script)._
