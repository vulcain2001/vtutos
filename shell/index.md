# vtutos / **shell**

This folder contains tutorials concerning **[_Shell_ scripts](https://en.wikipedia.org/wiki/Shell_script)** on _Linux_.

![WTFPL Badge](../.ressources/wtfpl-badge.png)

## Tutorials

- _**[luks](./luks/) :** embedding **[LUKS](https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup)** in a script._
