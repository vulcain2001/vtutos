#!/bin/sh

# vtutos/shell/luks/luks.sh
# (c) 2024 vulcain - WTPL V2
# from https://gitlab.com/vulcain2001/vtutos/

# CONFIGURATION
CRYPTSETUP=cryptsetup       # cryptsetup path.
DEVICE=/dev/sda             # Device's path.
MOUNTPOINT="/media/${USER}" # Root directory for mount point.
UID=$(id -u)                # Mount point owner's user id.
GID=$UID                    # Mount point owner's group id.
UMASK=0077                  # Users' permissions mask for mount point.

# Checking configuration.
if [ ! -e "${DEVICE}" ]; then
	echo Device "${DEVICE}" not found.
	exit 1
fi

# OPENING
# Getting device's UUID.
UUID=$(sudo "${CRYPTSETUP}" luksUUID "${DEVICE}")
echo Opening "${UUID}"...
# Opening vault.
sudo "${CRYPTSETUP}" open "${DEVICE}" "${UUID}" || exit $?
# Creating mount point.
MOUNTPOINT="${MOUNTPOINT}/${UUID}"
sudo mkdir -p "${MOUNTPOINT}"
# Mounting vault.
sudo mount -o "uid=${UID},gid=${GID},umask=${UMASK}" "/dev/mapper/${UUID}" "${MOUNTPOINT}" || exit $?

# Define your own process here !
echo "${UUID} sucessfully opened."
read -p "Press <Enter> to close it" REPLY

# CLOSING
echo Closing "${UUID}"...
# Unmounting vault.
sudo umount "${MOUNTPOINT}" || exit $?
# Removing mount point.
sudo rmdir "${MOUNTPOINT}" || exit $?
# Closing vault.
sudo "${CRYPTSETUP}" close "${UUID}" || exit $?