# vtutos / shell / **luks**

This tutorial will help you to embed **disk encryption in a [_Shell_ script](https://en.wikipedia.org/wiki/Shell_script) using _[LUKS](https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup)_**.

> You can download a **configurable version** of the script [here](./luks.sh).

![WTFPL Badge](../../.ressources/wtfpl-badge.png)

- [vtutos / shell / **luks**](#vtutos--shell--luks)
  - [Requirements](#requirements)
    - [Creating the _LUKS_ device](#creating-the-luks-device)
  - [Opening and closing the device](#opening-and-closing-the-device)
  - [Adding a filesystem](#adding-a-filesystem)
    - [Creating it](#creating-it)
    - [Mounting it](#mounting-it)
      - [Fixing permissions](#fixing-permissions)

---

## Requirements

Make sure _[cryptsetup](https://gitlab.com/cryptsetup/cryptsetup)_ is installed on your _Linux_ system : **open a terminal and type `sudo cryptsetup --version`**. If it can't be found, refer to the [project wiki](https://gitlab.com/cryptsetup/cryptsetup/-/wikis/home#download) for installation.

### Creating the _LUKS_ device

1. In a terminal, **find the path of the device** you want to encrypt using `sudo fdisk -l`. From now, we will use `/dev/sda`.
2. _(Optional)_ To **securely erase all data from the device**, you can use :
   1. `sudo dd if=/dev/urandom of=/dev/sda` (_most secure_) ;
   2. `sudo badblocks -c 10240 -s -w -t random -v /dev/sda` (_faster_).
3. **Create your _LUKS_ device** using `sudo cryptsetup luksFormat /dev/sda`. Your password must be at least 8 characters long and not be a dictionary word.

---

## Opening and closing the device

1. **Create a new script** with `vim luks.sh` and add mandatory line : `#!/bin/sh`.
2. **Put the device's path** in a variable using `DEVICE=/dev/sda`
3. **Save the device's UUID** in a variable with `UUID=$(sudo cryptsetup luksUUID "${DEVICE}")`.
4. To **open the _LUKS_ device**, use `sudo cryptsetup open "${DEVICE}" "${LABEL}"`. You can now find the file corresponding to your device in `/dev/mapper/${UUID}`.
5. For **testing** purpose, add a dummy process line : `read -p "${LABEL} opened."`.
6. Finally, to **close the device**, use `sudo cryptsetup close "${LABEL}"`.
7. Close _Vim_ and **add running permission** to the script using `chmod +x luks.sh`.

---

## Adding a filesystem

### Creating it

1. **Open your _LUKS_ device** using the script : `./luks.sh`.
2. In a **new terminal**, **get and save the device's UUID** in a variable with `export UUID=$(sudo cryptsetup luksUUID /dev/sda)`.
3. **Create a new filesystem** on your device using :
   1. `mkfs.vfat /dev/mapper/${UUID}` for a [_**FAT-32**_ filesystem](https://en.wikipedia.org/wiki/File_Allocation_Table) (_best compatibility_) ;
   2. `mkfs.ext4 /dev/mapper/${UUID}` for an [_**ext4**_ filesystem](https://en.wikipedia.org/wiki/Ext4) (_better for Linux_) ;
   3. `mkfs.ntfs /dev/mapper/${UUID}` for a [_**NTFS**_ filesystem](https://en.wikipedia.org/wiki/NTFS) (_better for Windows_).
4. Close the new terminal and **end the script** in the old one by pressing _Enter_.

### Mounting it

1. Open your script with _Vim_ and add a variable to **store the mount point's path** : `MOUNTPOINT="/media/${USER}/${UUID}"`. We use the device's UUID as filename to guarantee uniqueness.
2. After the [opening command](#opening-and-closing-the-device), **create the mount point** with `sudo mkdir -p "${MOUNTPOINT}"`.
3. Then, add the following line to **mount the device** : `sudo mount "/dev/mapper/${UUID}" "${MOUNTPOINT}"`.
4. Before the [closing command](#opening-and-closing-the-device), **unmount the device** with `sudo umount "${MOUNTPOINT}"`.
5. **Remove the mount point** to prevent collisions using `sudo rmdir "${MOUNTPOINT}"`.

#### Fixing permissions

1. To **make the current user owner of the mount point** (_default owner is `root`_), add following options to the [mount command](#mounting-it) : `-o "uid=${UID},gid=${UID},umask=${UMASK}"`.
2. **Store the current user's id** in a variable (_before the mounting_) using `UID=$(id -u)`.
3. Likewise, **define user's permissions mask** with `UMASK=0077` (_read, write and execution allowed for owner only_).
