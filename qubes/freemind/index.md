# vtutos / qubes / **freemind**

This tutorial will help you to install _**[FreeMind](https://freemind.sourceforge.io/wiki/)**_ on _**[Qubes OS](https://www.qubes-os.org/)**_ using a _Debian_ qube.

![WTFPL Badge](../../.ressources/wtfpl-badge.png)

- [vtutos / qubes / **freemind**](#vtutos--qubes--freemind)
  - [Install requirements](#install-requirements)
  - [Install _FreeMind_](#install-freemind)
  - [Create a desktop shortcut](#create-a-desktop-shortcut)

---

## Install requirements

1. Make sure _Java_ is installed on your qube : **open a terminal and type `java --version`**. If it's already installed, skip to [_FreeMind_ installation](#install-freemind).
2. To **install the _Java Runtime Environment_**, use `sudo apt install -y default-jre`.

## Install _FreeMind_

1. **[Download](https://sourceforge.net/projects/freemind/files/latest/download) _FreeMind_ binaries** and place it wherever in the qube.
2. To **unzip the archive**, use `sudo unzip -d /opt/freemind freemind-bin-max-1.0.1.zip`.
3. Make the **main script runnable** with `chmod +x /opt/freemind/freemind.sh`.
4. Add an **easy-access shortcut** to the script using `sudo ln -s /opt/freemind/freemind.sh /usr/bin/freemind`. You can now **launch _FreeMind_ by typing `freemind` in a terminal**.

## Create a desktop shortcut

1. To use the _FreeMind_'s icon, **download it from [here](./freemind.png)** and place it in the `/opt/freemind` directory.
2. **[Download](./freemind.desktop) the desktop file** and place it in `/usr/share/applications`.
3. In a **_dom0_ terminal**, synchronize the shortcuts with `qvm-sync-appmenus <qube name>`. You should now be able to **add it to the applications menu** in the qube's settings.
