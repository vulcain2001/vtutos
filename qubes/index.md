# vtutos / **qubes**

This folder contains tutorials concerning _**[Qubes OS](https://www.qubes-os.org/)**_.

![WTFPL Badge](../.ressources/wtfpl-badge.png)

## Tutorials

- _**[freemind](./freemind/) :** installing **[FreeMind](https://freemind.sourceforge.io/wiki/)**._
- _**[sys-protonvpn](./sys-protonvpn/) :** creating a **[ProtonVPN](https://protonvpn.com/) netqube**._
