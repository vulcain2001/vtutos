# vtutos / qubes / **sys-protonvpn**

This tutorial will help you to create a **_Fedora_ netqube providing a VPN connection** using _[ProtonVPN](https://protonvpn.com/)_.

![WTFPL Badge](../../.ressources/wtfpl-badge.png)

- [vtutos / qubes / **sys-protonvpn**](#vtutos--qubes--sys-protonvpn)
  - [Create a new netqube](#create-a-new-netqube)
  - [Install _ProtonVPN_](#install-protonvpn)
    - [Using the CLI](#using-the-cli)
    - [Using the desktop app](#using-the-desktop-app)
  - [Starting the VPN automatically](#starting-the-vpn-automatically)
  - [Boosting security](#boosting-security)
    - [Create a VPN firewall](#create-a-vpn-firewall)
    - [Use it by default](#use-it-by-default)

---

## Create a new netqube

1. In a **_dom0_ terminal**, use `qvm-create --standalone --template fedora-36 --label purple sys-vpn` to create a **_Fedora_-based standalone qube**[^standalone]. This may take some times...
2. Depending on your desired configuration, use one of this command to **set the network provider** for our qube :
   1. `qvm-prefs --set sys-vpn netvm sys-firewall` to use your **firewall configuration** ;
   2. `qvm-prefs --set sys-vpn netvm sys-net` to bypass your firewall (**_not recommended**_).
3. **Make it a netqube** using `qvm-prefs --set sys-vpn provides_network true`.

[^standalone]: You can alternatively create a **VPN _TemplateVM_** and use an _AppVM_ as VPN provider.

## Install _ProtonVPN_

### Using the CLI

1. Open a terminal (_in the VPN netqube_) and **install the _ProtonVPN_ CLI** using `sudo dnf install -y protonvpn-cli`.
2. Use `protonvpn-cli login <username>` to **setup your credentials**.

### Using the desktop app

1. Open a terminal (_in the VPN netqube_) and **download _ProtonVPN_ RPM package** using `wget https://repo.protonvpn.com/fedora-39-stable/protonvpn-stable-release/protonvpn-stable-release-1.0.1-2.noarch.rpm`.
2. **Install _ProtonVPN_ desktop app** by typing `sudo dnf install -y protonvpn`.
3. Use `protonvpn` to launch the application and **setup your credentials**.
4. _(Optional)_ **Remove the RPM package** with `rm protonvpn-stable-release-1.0.1-2.noarch.rpm`.

> For further informations, see [_ProtonVPN_ official support](https://protonvpn.com/support/official-linux-vpn-fedora/).

## Starting the VPN automatically

1. To **start automatically _ProtonVPN_ on the qube startup**, you can :
   1. **[Download](./protonvpn-cli.desktop) this desktop file** and place it in `/home/user/.config/autostart`[^autostart] if you want to use the CLI ;
   2. Create a **symbolic link to _ProtonVPN_ in the autostart folder[^autostart]**, using `ln -s /usr/share/applications/protonvpn.desktop /home/user/.config/autostart/`, if you want to use the GUI (_slower to start_).
2. In a _dom0_ terminal, type `qvm-prefs --set sys-vpn autostart true` to **start automatically the VPN netqube on _dom0_ startup**.

[^autostart]: Create the `autostart` directory if doesn't exist.

---

## Boosting security

### Create a VPN firewall

1. Open a _dom0_ terminal and use `qvm-create --disp --template fedora-36-dvm --label purple sys-vpn-firewall` to create a **VPN firewall netqube**.
2. Type `qvm-prefs --set sys-vpn-proton-vpn netvm sys-vpn` to use the **VPN qube as network provider**.
3. To **make it a netqube**, use `qvm-prefs --set sys-vpn-firewall provides_network true`.

### Use it by default

1. To use it as **default network provider**, type `qubes-prefs --set default_netvm sys-vpn-firewall` in a _dom0_ terminal.
2. Make it **start automatically on _dom0_ startup** using `qvm-prefs --set sys-vpn-firewall autostart true`.
